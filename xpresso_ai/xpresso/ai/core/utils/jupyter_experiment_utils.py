import os
from matplotlib import pyplot
from xpresso.ai.core.commons.exceptions.xpr_exceptions import MountPathNoneType

# constants
XPRESSO_SAVE_PLOT = "xpresso_save_plot"
DEFAULT_IMAGE_PATH = "./images"


def xpresso_save_plot(fig_id, output_path=DEFAULT_IMAGE_PATH,
                      output_folder=None, fig_extension="png",
                      resolution=300, tight_layout=True):
    """
    Saves matplotlib plot into specified output path
    Args:
    """
    if not output_path:
        raise MountPathNoneType
    if output_folder:
        output_path = os.path.join(output_path, output_folder)
    file_name = f"{fig_id}.{fig_extension}"
    path = os.path.join(output_path, file_name)
    print(f"Xpresso saving figure to {path}")
    if not os.path.exists(output_path):
        os.makedirs(output_path)
    if tight_layout:
        pyplot.tight_layout()
    pyplot.savefig(path, format=fig_extension, dpi=resolution)
